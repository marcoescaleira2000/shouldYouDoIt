[![pipeline status](https://gitlab.com/marcoescaleira2000/shouldYouDoIt/badges/develop/pipeline.svg)](https://gitlab.com/marcoescaleira2000/shouldYouDoIt/commits/develop) [![coverage report](https://gitlab.com/marcoescaleira2000/shouldYouDoIt/badges/develop/coverage.svg)](https://gitlab.com/marcoescaleira2000/shouldYouDoIt/commits/develop)

# Should You do It

## Ask us :) We will answer your amazing question

## A small project based on *React.js* using the
REST API: https://shouldyoudoit.herokuapp.com/ / https://github.com/MarceloSilva00/shouldYouDoIt_BE

#### Environments

 - QA: https://shouldyoudoit-qa.surge.sh/
 - PROD: https://shouldyoudoit.surge.sh/
