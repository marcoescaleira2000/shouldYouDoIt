import React, { Fragment, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";

import HomePage from "~pages/HomePage/HomePage";
import GalleryPage from "~pages/GalleryPage/GalleryPage";
import Header from "~components/Header/Header";
import Footer from "~components/Footer/Footer";
import NotFound from "~components/NotFound/NotFound";

import { AppContainer } from "./styled-components";

import { actions as appActions } from "~store/app";
import { UnaryFn } from "~utils/functionalTypes";

type Props = {
  setIsMobile: UnaryFn<boolean, void>;
};

const AppRouter: React.FC<Props> = ({ setIsMobile }) => {
  useEffect(() => {
    setIsMobile(isMobile);
  }, [isMobile]);

  return (
    <BrowserRouter>
      <Fragment>
        <AppContainer isMobile={isMobile}>
          <Header />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/gallery" component={GalleryPage} />
            <Route component={NotFound} />
          </Switch>
        </AppContainer>
        <Footer />
      </Fragment>
    </BrowserRouter>
  );
};

const mapDispatchToProps = {
  setIsMobile: appActions.changeIsMobile
};

export default connect(null, mapDispatchToProps)(AppRouter);
