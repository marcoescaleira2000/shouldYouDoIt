import styled from "styled-components";
import { footerHeight } from "~/styles/_settings";

type AppContainerProps = {
  isMobile: boolean;
};

export const AppContainer = styled.div<AppContainerProps>`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  margin-bottom: ${props => (props.isMobile ? footerHeight : "0")};
`;
