import { Dispatch } from "redux";
import { ActionTypes } from "../actionTypes";

export interface ChangeIsMobile {
  type: ActionTypes.SET_IS_MOBILE;
  payload: boolean;
}

export type AppActions = ChangeIsMobile;

export const changeIsMobile = (isMobile: boolean) => (
  dispatch: Dispatch<AppActions>
): void => {
  dispatch({
    type: ActionTypes.SET_IS_MOBILE,
    payload: isMobile
  });
};
