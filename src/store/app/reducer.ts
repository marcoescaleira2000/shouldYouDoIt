import { ActionTypes } from "../actionTypes";
import { AppActions } from "./actions";
import { AppState } from ".";

export const defaultAppState: AppState = {
  isMobile: false
};

const decisionsReducer = (
  state = defaultAppState,
  action: AppActions
): AppState => {
  switch (action.type) {
    case ActionTypes.SET_IS_MOBILE:
      return {
        isMobile: action.payload
      };
    default:
      return state;
  }
};

export default decisionsReducer;
