import * as reducer from "./reducer";
import * as actions from "./actions";
import * as selectors from "./selectors";

export interface AppState {
  isMobile: boolean;
}

export { actions, selectors, reducer };
export default reducer.default;
