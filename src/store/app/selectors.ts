import { StoreState } from "..";

export const getIsMobile = (state: StoreState) => state.app.isMobile;
