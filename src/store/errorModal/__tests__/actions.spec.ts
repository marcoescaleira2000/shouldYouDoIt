import { ActionTypes } from "~/store/actionTypes";
import { openModal, closeModal } from "../actions";

describe("Error modal actions tests", () => {
  const dispatchSpy = jest.fn();

  test("It should open modal", () => {
    const message = "message";
    openModal(message)(dispatchSpy);

    expect(dispatchSpy).toHaveBeenCalledWith({
      type: ActionTypes.OPEN_MODAL,
      payload: {
        message
      }
    });
  });

  test("It should close modal", () => {
    closeModal()(dispatchSpy);

    expect(dispatchSpy).toHaveBeenCalledWith({
      type: ActionTypes.CLOSE_MODAL
    });
  });
});
