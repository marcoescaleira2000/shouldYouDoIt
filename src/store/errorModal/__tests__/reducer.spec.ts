import { ActionTypes } from "~/store/actionTypes";
import decisionsReducer from "../reducer";

describe("Error modal reducer tests", () => {
  test("It should open modal", () => {
    const message = "message";
    expect(
      decisionsReducer(undefined, {
        type: ActionTypes.OPEN_MODAL,
        payload: {
          message
        }
      })
    ).toEqual({
      isOpen: true,
      message
    });
  });

  test("It should close modal", () => {
    expect(
      decisionsReducer(undefined, {
        type: ActionTypes.CLOSE_MODAL
      })
    ).toEqual({
      isOpen: false,
      message: ""
    });
  });
});
