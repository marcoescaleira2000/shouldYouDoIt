import React from "react";
import { connect } from "react-redux";
import { StoreState } from "~store";
import { Container, Title, Image, Loader } from "./styled-components";
import { selectors, DecisionsStateData } from "~store/decisions";
import { NullaryFn } from "~utils/functionalTypes";
// @ts-ignore
import loadingGif from "~assets/gifs/loading.gif";

export interface Props {
  resetData: NullaryFn<void>;
  data: DecisionsStateData;
}

const imageVariants = {
  visible: { scale: 1 },
  hidden: { scale: 0.5 }
};

const Answer: React.FC<Props> = ({ data: { msg, img } }) => (
  <Container>
    <Title>{msg || "Preparing your answer..."}</Title>
    {img ? (
      <Image
        src={img}
        alt="Should You Do It gif"
        initial="hidden"
        animate="visible"
        variants={imageVariants}
        transition={{ duration: 0.6 }}
      />
    ) : (
      <Loader src={loadingGif} alt="Should You Do It gif" />
    )}
  </Container>
);

Answer.defaultProps = {
  data: { msg: "loading", img: "" }
};

const mapStateToProps = (state: StoreState) => ({
  data: selectors.getData(state)
});

// @ts-ignore
export default connect(mapStateToProps)(Answer);
