import styled from "styled-components";
import { motion } from "framer-motion";
import { sizes, pallete } from "~styles/_settings";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: ${sizes.s} 0 1rem 0;
  max-width: 30rem;

  @media (min-width: ${sizes.desktop_breakpoint}) {
    max-width: 50rem;
  }
`;

export const Title = styled.h1`
  color: ${pallete.lightest};
  margin-top: 0;
  font-size: 2rem;
  border-left: 0.3rem solid ${pallete.lightest};
  border-right: 0.3rem solid ${pallete.lightest};
  text-align: center;
  text-transform: capitalize;

  @media (min-width: ${sizes.desktop_breakpoint}) {
    font-size: 2.6rem;
  }
`;

export const Image = styled(motion.img)`
  width: 30rem;
  height: 25rem;
  border-radius: 4px;
  margin-bottom: ${sizes.m};

  @media (min-width: ${sizes.desktop_breakpoint}) {
    width: 50rem;
    height: 42rem;
  }
`;

export const Loader = styled(motion.img)`
  width: 30rem;
  height: 25rem;
  margin-bottom: ${sizes.m};

  @media (min-width: ${sizes.desktop_breakpoint}) {
    width: 50rem;
    height: 42rem;
  }
`;
