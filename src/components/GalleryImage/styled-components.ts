import styled from "styled-components";
import { sizes } from "~/styles/_settings";

export const Image = styled.img`
  width: 12rem;
  height: 12rem;
  margin: 1rem;
  margin-bottom: 0;

  @media (min-width: ${sizes.desktop_breakpoint}) {
    width: 16rem;
    height: 16rem;
  }
`;
