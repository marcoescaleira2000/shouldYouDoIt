import React from "react";
import { renderWithRouter } from "~/utils/test-utils";
import { Header } from "../Header";

describe("Header test suite", () => {
  it("Should render Header correctly mobile", () => {
    const { container } = renderWithRouter(<Header isMobile />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should render Header correctly non-mobile", () => {
    const { container } = renderWithRouter(<Header isMobile={false} />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
