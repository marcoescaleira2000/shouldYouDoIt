import React, { useMemo } from "react";
import { connect } from "react-redux";
import {
  Container,
  Nav,
  List,
  Item,
  Link,
  TitleSpan
} from "./styled-components";
import { StoreState } from "~store";
import { getIsMobile } from "~store/app/selectors";

type Props = {
  isMobile: boolean;
};

export const Header: React.FC<Props> = ({ isMobile }) =>
  useMemo(
    () => (
      <Container>
        <Nav isMobile={isMobile}>
          <Link to="/" exact isTitle>
            Should You <TitleSpan>Do It</TitleSpan>
          </Link>
          {!isMobile && (
            <List>
              <Item>
                <Link
                  to="/"
                  exact
                  isNavLink
                  activeClassName="headerActiveHeaderLink"
                >
                  Home
                </Link>
              </Item>
              <Item>
                <Link
                  to="/gallery"
                  isNavLink
                  activeClassName="headerActiveHeaderLink"
                >
                  Gallery
                </Link>
              </Item>
            </List>
          )}
        </Nav>
      </Container>
    ),
    [isMobile]
  );

Header.defaultProps = {
  isMobile: false
};

const mapStateToProps = (state: StoreState) => ({
  isMobile: getIsMobile(state)
});

export default connect(mapStateToProps)(Header);
