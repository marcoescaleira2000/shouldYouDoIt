import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import { UnaryFn, NullaryFn, BinaryFn } from "~/utils/functionalTypes";
import { actions as decisionsActions, selectors } from "~/store/decisions";
import { actions as errorModalActions } from "~/store/errorModal";
import { API_URL } from "~/config";
import { StoreState } from "~/store";

import {
  Container,
  Title,
  FormContainer,
  Input,
  ButtonContainer,
  Submit,
  CleanButton
} from "./styled-components";

type SetDataType = BinaryFn<string, string, void>;
type ResetDataType = NullaryFn<void>;
type SetLoadingAnimationType = NullaryFn<void>;

type Props = {
  setData: SetDataType;
  resetData: ResetDataType;
  setLoadingAnimation: SetLoadingAnimationType;
  oldTextInput: string;
  openErrorModal: UnaryFn<string, void>;
  isPrinting: boolean;
};

export const request = async (textInput: string, setData: SetDataType) => {
  const response = await fetch(`${API_URL}/?search=${textInput.trim()}`, {
    headers: {
      "Content-Type": "application/json"
    }
  });
  const data = await response.json();

  setData(data, textInput);
};

export const handleTextInput = (
  textInput: string,
  setTextInput: UnaryFn<string, void>
) => {
  if (!textInput || /^[a-z][a-z\s]*$/.test(textInput)) {
    setTextInput(textInput);
  }
};

export const handleFormSubmit = (
  event: any,
  resetData: ResetDataType,
  textInput: string,
  oldTextInput: string,
  setLoadingAnimation: SetLoadingAnimationType,
  openErrorModal: UnaryFn<string, void>,
  setData: SetDataType,
  setTextInput: UnaryFn<string, void>
) => {
  event.preventDefault();
  resetData();

  if (!textInput) {
    openErrorModal("Please enter what you want to do.");
    return;
  }

  if (oldTextInput === textInput) {
    openErrorModal("Please do another thing!");
    setTextInput("");
    return;
  }

  setLoadingAnimation();

  request(textInput, setData);
};

const Form: React.FC<Props> = ({
  resetData,
  oldTextInput,
  setLoadingAnimation,
  openErrorModal,
  setData,
  isPrinting
}) => {
  const [textInput, setTextInput] = useState<string>("");

  return (
    <Fragment>
      <Container>
        <Title>What do you want to do?</Title>
        <FormContainer
          onSubmit={(e: any) =>
            handleFormSubmit(
              e,
              resetData,
              textInput,
              oldTextInput,
              setLoadingAnimation,
              openErrorModal,
              setData,
              setTextInput
            )
          }
          autoComplete="off"
        >
          <Input
            type="text"
            name="toDo"
            value={textInput}
            onChange={(e: any) => handleTextInput(e.target.value, setTextInput)}
            data-testid="input"
          />
          {isPrinting ? (
            <ButtonContainer>
              <CleanButton onClick={resetData} whileTap={{ scale: 0.9 }}>
                Clean
              </CleanButton>
              <Submit type="submit" whileTap={{ scale: 0.9 }}>
                Decide
              </Submit>
            </ButtonContainer>
          ) : (
            <Fragment>
              <Submit type="submit" whileTap={{ scale: 0.9 }}>
                Decide
              </Submit>
            </Fragment>
          )}
        </FormContainer>
      </Container>
    </Fragment>
  );
};

const mapStateToProps = (state: StoreState) => ({
  oldTextInput: selectors.getOldTextInput(state),
  isPrinting: selectors.getPrint(state)
});

const mapDispatchToProps = {
  resetData: decisionsActions.resetData,
  setLoadingAnimation: decisionsActions.setLoadingAnimation,
  setData: decisionsActions.setData,
  openErrorModal: errorModalActions.openModal
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Form);
