import styled from "styled-components";
import { motion } from "framer-motion";
import { sizes, pallete, normalFont } from "~/styles/_settings";

export const Container = styled.div`
  position: relative;
  text-align: center;
  display: flex;
  flex-direction: column;
  width: 30rem;
  font-family: ${normalFont};

  @media (min-width: ${sizes.desktop_breakpoint}) {
    width: 50rem;
  }
`;

export const Title = styled.h1`
  color: ${pallete.sixth_light};
  font-size: 2.5rem;

  @media (min-width: ${sizes.desktop_breakpoint}) {
    font-size: 3rem;
  }
`;

export const FormContainer = styled.form`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export const Input = styled.input`
  height: ${sizes.xl};
  flex-grow: 1;
  background-color: ${pallete.second_dark};
  border: none;
  border-radius: 1rem;
  color: ${pallete.white};
  font-size: 1.3rem;
  padding: ${sizes.s};
  margin-bottom: ${sizes.s};
  outline: none;
  font-family: ${normalFont};
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const Submit = styled(motion.button)`
  height: 4rem;
  flex-grow: 1;
  background-color: ${pallete.fourth_dark};
  border: none;
  border-radius: 1rem;
  color: ${pallete.white};
  font-size: 1.5rem;
  margin-bottom: 1.5rem;
  outline: none;

  &:hover {
    filter: brightness(90%);
  }
`;

export const CleanButton = styled(motion.button)`
  flex-grow: 1;
  background-color: ${pallete.third_dark};
  font-size: 1.5rem;
  height: 4rem;
  color: ${pallete.white};
  border: none;
  border-radius: 1rem;
  margin-right: 1rem;
  outline: none;

  &:hover {
    filter: brightness(90%);
  }
`;
