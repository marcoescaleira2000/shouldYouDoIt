import React, { useMemo } from "react";
import { connect } from "react-redux";
import { MdHome, MdCollections } from "react-icons/md";
import {
  DesktopContainer,
  MobileContainer,
  MobileNav,
  MobileLink,
  Text,
  NameLink,
  Heart
} from "./styled-components";
import { StoreState } from "~store";
import { getIsMobile } from "~store/app/selectors";

const menuItems: Array<{
  name: string;
  to: string;
  exact?: boolean;
  icon: JSX.Element;
}> = [
  {
    name: "Home",
    to: "/",
    exact: true,
    icon: <MdHome />
  },
  {
    name: "Gallery",
    to: "/gallery",
    exact: false,
    icon: <MdCollections />
  }
];

type Props = {
  isMobile: boolean;
};

export const Footer: React.FC<Props> = ({ isMobile }) =>
  useMemo(
    () =>
      isMobile ? (
        <MobileContainer>
          <MobileNav>
            {menuItems.map(({ name, exact, to, icon }) => (
              <MobileLink
                to={to}
                exact={exact}
                activeClassName="footerActiveHeaderLink"
                key={name}
              >
                {icon}
                {name}
              </MobileLink>
            ))}
          </MobileNav>
        </MobileContainer>
      ) : (
        <DesktopContainer>
          <Text>
            Made with <Heart>&#x2665;</Heart> by&nbsp;
            <NameLink href="https://gitlab.com/marcoescaleira2000">
              Marco Escaleira
            </NameLink>
          </Text>
        </DesktopContainer>
      ),
    [isMobile]
  );

Footer.defaultProps = {
  isMobile: false
};

const mapStateToProps = (state: StoreState) => ({
  isMobile: getIsMobile(state)
});

export default connect(mapStateToProps)(Footer);
