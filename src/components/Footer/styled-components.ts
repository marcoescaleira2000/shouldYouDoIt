import styled, { css } from "styled-components";
import { NavLink } from "react-router-dom";
import {
  sizes,
  pallete,
  footerHeight,
  footerDesktopHeight,
  normalFont
} from "~/styles/_settings";

const containerStyles = css`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: ${footerHeight};
  font-family: ${normalFont};

  @media (min-width: ${sizes.desktop_breakpoint}) {
    height: ${footerDesktopHeight};
  }
`;

export const DesktopContainer = styled.footer`
  ${containerStyles}
`;

export const MobileContainer = styled.footer`
  ${containerStyles}
  position: fixed;
  bottom: 0;
  background-color: rgba(29, 154, 108, 1);
`;

export const MobileNav = styled.nav`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const MobileLink = styled(NavLink)`
  min-width: 5rem;
  color: ${pallete.second_light};
  font-size: 1.6rem;
  margin-right: 4rem;
  display: flex;
  align-items: center;
  outline: none;

  svg {
    width: 1.8rem;
    height: 1.8rem;
    flex-shrink: 0;
    margin-right: 0.4rem;
  }

  &:active {
    color: ${pallete.fifth_light};
  }

  &:last-child {
    margin-right: 0;
  }
`;

export const Text = styled.p`
  text-align: center;
  color: ${pallete.second_light};
  align-items: center;
  font-size: 1.4rem;

  @media (min-width: ${sizes.desktop_breakpoint}) {
    font-size: ${sizes.m};
  }
`;

export const NameLink = styled.a`
  text-decoration: underline;
  letter-spacing: 0.15rem;
  color: ${pallete.second_light};
  font-weight: 500;
  outline: none;
`;

export const Heart = styled.span`
  font-size: 2.2rem;
  color: red;
`;
