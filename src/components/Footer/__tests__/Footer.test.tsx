import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Footer } from "../Footer";

describe("Footer test suite", () => {
  it("Should render Footer correctly non mobile", () => {
    const { container } = render(<Footer isMobile={false} />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should render Footer correctly mobile", () => {
    const { container } = render(
      <BrowserRouter>
        <Footer isMobile />
      </BrowserRouter>
    );

    expect(container.firstChild).toMatchSnapshot();
  });
});
