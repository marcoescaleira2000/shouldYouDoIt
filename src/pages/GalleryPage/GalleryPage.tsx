import React, { useState, useEffect } from "react";
import { API_URL } from "~/config";
import { DecisionsStateData } from "~/store/decisions";

import GalleryImage from "~components/GalleryImage/GalleryImage";
import {
  Container,
  Title,
  Body,
  DoIt,
  DontDoIt,
  SubTitle,
  List
} from "./styled-components";

type RequestedDecisionsStateData = Array<DecisionsStateData>;

const Gallery: React.FC = () => {
  const [doIt, setDoIt] = useState<RequestedDecisionsStateData>();
  const [dontDoIt, setDontDoIt] = useState<RequestedDecisionsStateData>();

  useEffect(() => {
    const requestAll = async () => {
      const response = await fetch(`${API_URL}/all`);
      const data: RequestedDecisionsStateData = await response.json();

      const doit: RequestedDecisionsStateData = data.filter(
        answer => answer.msg === "do it"
      );
      const dontdoit: RequestedDecisionsStateData = data.filter(
        answer => answer.msg === "don't do it"
      );

      setDoIt(doit);
      setDontDoIt(dontdoit);
    };
    requestAll();
  }, []);

  return (
    <Container>
      <Title>Gallery</Title>

      <Body>
        <DoIt>
          <SubTitle>Do It</SubTitle>
          <List>
            {doIt?.map((answer, index) => (
              <GalleryImage key={index} answer={answer} />
            ))}
          </List>
        </DoIt>
        <DontDoIt>
          <SubTitle>Don't Do It</SubTitle>
          <List>
            {dontDoIt?.map((answer, index) => (
              <GalleryImage key={index} answer={answer} />
            ))}
          </List>
        </DontDoIt>
      </Body>
    </Container>
  );
};

export default Gallery;
